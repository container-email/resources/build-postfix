#!/bin/sh

: "${PACKAGER:="Docker Builder <builder@builder>"}"
echo "Using packager: $PACKAGER"

if [ -z "$PUBKEY" ] || [ -z "$PRIVKEY" ]; then
  echo "PUBKEY and PRIVKEY need to be set"
  exit 1
fi
mkdir abuild
echo "PACKAGER=\"${PACKAGER}\"" >abuild/abuild.conf
echo "PACKAGER_PRIVKEY=\"/home/devuser/.abuild/$KEYNAME\"" >abuild/abuild.conf
echo "$PRIVKEY" >abuild/"$KEYNAME"
chmod 600 abuild/"$KEYNAME"
echo "$PUBKEY" >abuild/"$KEYNAME".pub

IMG="registry.gitlab.com/a16bitsysop/alpine-dev-local/main:$BRANCH"

docker container run --platform=linux/"$BUILD_ARCH" -v $PWD/$PNAME:/aport -v $PWD/deploy:/deploy -v $PWD/abuild:/home/devuser/.abuild -v $PWD/abuild/$KEYNAME.pub:/etc/apk/keys/$KEYNAME.pub "$IMG" build-aport.sh
